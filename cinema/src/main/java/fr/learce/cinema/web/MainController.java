package fr.learce.cinema.web;

import fr.learce.cinema.dao.FilmsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
    @Autowired
    FilmsDao filmsDao;

    @GetMapping("/")
    public String main(Model model){
        model.addAttribute("nom", "nom");
        model.addAttribute("films", filmsDao.films());
        return "index";
    }
    /*detail du film*/
    @GetMapping("/film/{id}")
    /*on recupere le paramettre ID*/
    public String detail(Model model, @PathVariable("id") String id){
        /*transformation du string en integer*/
        Integer idFilm =    Integer.parseInt(id);
        /*ajouter le id dans le film*/
        model.addAttribute("film", filmsDao.getById(idFilm));
        return"detail";
    }
    @GetMapping("/afficheImage/{id}")
    public String afficheImage(Model model,@PathVariable("id") String id){
        Integer idFilm =    Integer.parseInt(id);
        model.addAttribute("film",filmsDao.getById(idFilm));
        return "afficheImage";
    }
}
